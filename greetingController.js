let greetingCounter = 0;

const createGreeting = (name = 'World') => {
  greetingCounter++;
  return {
    id: greetingCounter,
    content: `Hello ${name}!`,
  };
};

const greetingController = (req, res) => {
  const name = req.query.name;

  const greeting = createGreeting(name);

  res.send(greeting);
};

module.exports = greetingController;
