const express = require('express');
const cors = require('cors');
const PORT = 4000;

const greetingController = require('./greetingController');

const app = express();
app.use(express.json());
app.use(cors());

app.get('/greeting', greetingController);

app.listen(PORT, () => {
  console.log(`rest-example-node listening on port ${PORT}!`);
});
